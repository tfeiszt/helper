<?php

namespace tfeiszt\helper;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class Helper
 * @package tfeiszt\helper
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class Helper {

    /**
     * @param string $env
     */
    public static function setEnv($env = '')
    {
        switch ($env) {
            case 'dev' :
                error_reporting(E_ALL - E_STRICT);
                break;
            case 'staging' :
                error_reporting(E_ERROR);
                break;
            case 'production' :
                error_reporting(0);
                break;
            default :
                error_reporting(0);
                break;
        }
    }

    /**
     * @param $class
     * @return string
     */
    public static function getClassShortName($class)
    {
        $reflect = new \ReflectionClass($class);
        return $reflect->getShortName();
    }

    /**
     * @param string $message
     */
    public static function tr($message = '')
    {
        if (function_exists('xdebug_print_function_stack')) {
            xdebug_print_function_stack($message);
        } else {
            echo $message . '<hr>';
            debug_print_backtrace();
        }
        die();
    }

    /**
     * @param $input
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function camelize($input)
    {
        return strtr(ucwords(strtr($input, array('_' => ' ', '.' => '_ ', '\\' => '_ '))), array(' ' => ''));
    }

    /**
     * @param $input
     * @param string $glue
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function underscore($input, $glue = '_') {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode($glue, $ret);
    }

    /**
     * @return string
     */
    public static function getHttpMethod()
    {
        return (isset($_SERVER['REQUEST_METHOD'])) ? $_SERVER['REQUEST_METHOD'] : '';
    }

    /**
     * @param Request $request
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getHttpMethodByRequest(Request $request)
    {
        return $request->getMethod();
    }

    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getHttpHost()
    {
        return (isset($_SERVER['HTTP_HOST'])) ? $_SERVER['HTTP_HOST'] : '';
    }

    /**
     * @param Request $request
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getHttpHostByRequest(Request $request)
    {
        return $request->getHost();
    }

    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getProtocol()
    {
        if (static::isSecureProtocol()) {
            return 'https://';
        }
        return 'http://';
    }

    /**
     * @param Request $request
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getProtocolByRequest(Request $request)
    {
        if (static::isSecureProtocolByRequest($request)) {
            return 'https://';
        }
        return 'http://';
    }

    /**
     * @return bool
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function isSecureProtocol()
    {
        return (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? true : false;
    }

    /**
     * @param Request $request
     * @return bool
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function isSecureProtocolByRequest(Request $request)
    {
        return $request->isSecure();
    }

    /**
     * @param Request $request
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getHttpProtocolByRequest(Request $request)
    {
        return $request->getProtocolVersion();
    }

    /**
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getServerUrl()
    {
        return (isset($_SERVER['REQUEST_URI'])) ? $_SERVER['REQUEST_URI'] : '';
    }

    /**
     * @param Request $request
     * @return string
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getServerUrlByRequest(Request $request)
    {
        return $request->getRequestUri();
    }

    /**
     * @return bool
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function isCliRequest()
    {
        return (php_sapi_name() === 'cli' || defined('STDIN'));
    }

    /**
     * @return array
     */
    public static function getRequestHeaders()
    {
        $headers = array();
        $copyServer = array(
            'CONTENT_TYPE'   => 'Content-Type',
            'CONTENT_LENGTH' => 'Content-Length',
            'CONTENT_MD5'    => 'Content-Md5',
        );
        foreach ($_SERVER as $key => $value) {
            if (substr($key, 0, 5) === 'HTTP_') {
                $key = substr($key, 5);
                if (!isset($copyServer[$key]) || !isset($_SERVER[$key])) {
                    $key = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', $key))));
                    $headers[$key] = $value;
                }
            } elseif (isset($copyServer[$key])) {
                $headers[$copyServer[$key]] = $value;
            }
        }
        if (!isset($headers['Authorization'])) {
            if (isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION'])) {
                $headers['Authorization'] = $_SERVER['REDIRECT_HTTP_AUTHORIZATION'];
            } elseif (isset($_SERVER['PHP_AUTH_USER'])) {
                $basic_pass = isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : '';
                $headers['Authorization'] = 'Basic ' . base64_encode($_SERVER['PHP_AUTH_USER'] . ':' . $basic_pass);
            } elseif (isset($_SERVER['PHP_AUTH_DIGEST'])) {
                $headers['Authorization'] = $_SERVER['PHP_AUTH_DIGEST'];
            }
        }
        return $headers;
    }

    /**
     * @param Request $request
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getRequestHeadersByRequest(Request $request)
    {
        if ($request->headers) {
            return $request->headers->all();
        }
        return [];
    }

    /**
     * @param null $code
     * @param bool $headerFormat
     * @return string
     */
    public static function getResponseMessageByCode($code = null, $headerFormat = false)
    {
        if ($code !== null) {
            switch ($code) {
                case 100: $text = 'Continue'; break;
                case 101: $text = 'Switching Protocols'; break;
                case 200: $text = 'OK'; break;
                case 201: $text = 'Created'; break;
                case 202: $text = 'Accepted'; break;
                case 203: $text = 'Non-Authoritative Information'; break;
                case 204: $text = 'No Content'; break;
                case 205: $text = 'Reset Content'; break;
                case 206: $text = 'Partial Content'; break;
                case 300: $text = 'Multiple Choices'; break;
                case 301: $text = 'Moved Permanently'; break;
                case 302: $text = 'Moved Temporarily'; break;
                case 303: $text = 'See Other'; break;
                case 304: $text = 'Not Modified'; break;
                case 305: $text = 'Use Proxy'; break;
                case 400: $text = 'Bad Request'; break;
                case 401: $text = 'Unauthorized'; break;
                case 402: $text = 'Payment Required'; break;
                case 403: $text = 'Forbidden'; break;
                case 404: $text = 'Not Found'; break;
                case 405: $text = 'Method Not Allowed'; break;
                case 406: $text = 'Not Acceptable'; break;
                case 407: $text = 'Proxy Authentication Required'; break;
                case 408: $text = 'Request Time-out'; break;
                case 409: $text = 'Conflict'; break;
                case 410: $text = 'Gone'; break;
                case 411: $text = 'Length Required'; break;
                case 412: $text = 'Precondition Failed'; break;
                case 413: $text = 'Request Entity Too Large'; break;
                case 414: $text = 'Request-URI Too Large'; break;
                case 415: $text = 'Unsupported Media Type'; break;
                case 422: $text = 'Un-processable Entity'; break;
                case 428: $text = 'Precondition Failed'; break;
                case 429: $text = 'Too many requests'; break;
                case 431: $text = 'Request Header Fields Too Large'; break;
                case 451: $text = 'Unavailable For Legal Reasons'; break;
                case 500: $text = 'Internal Server Error'; break;
                case 501: $text = 'Not Implemented'; break;
                case 502: $text = 'Bad Gateway'; break;
                case 503: $text = 'Service Unavailable'; break;
                case 504: $text = 'Gateway Time-out'; break;
                case 505: $text = 'HTTP Version not supported'; break;
                default:
                    $text = 'Unknown http status code "' . htmlentities($code) . '"';
                    break;
            }
        } else {
            $code = ((isset($GLOBALS['http_response_code']) && ($GLOBALS['http_response_code'] !== null)) ? $GLOBALS['http_response_code'] : 200);

            return self::getResponseMessageByCode($code);
        }

        if ($headerFormat === true) {
            $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');

            return $protocol . ' ' . $code . ' ' . $text;
        } else {
            return $text;
        }
    }

    /**
     * @param $path
     * @return string
     */
    public static function setPathSlash($path)
    {
        if (substr($path, -1) != '/') {
            $path = $path . '/';
        }
        return $path;
    }

    /**
     * @param array $options
     */
    public static function sessionStart($options = [])
    {
        if (!(session_status() == PHP_SESSION_ACTIVE)) {
            session_start($options);
        }
    }

    /**
     * @param \Exception $e
     * @param string $file
     * @param null $line
     * @return array
     */
    public static function getExceptionContext(\Exception $e, $file = '', $line = null)
    {
        return  [
            'message' => $e->getMessage(),
            'code' => $e->getCode(),
            'file' => $file,
            'line' => $line
        ];
    }
}
