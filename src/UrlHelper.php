<?php

namespace tfeiszt\helper;

use Doctrine\Common\Collections\Criteria;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UrlHelper
 * @package tfeiszt\helper
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class UrlHelper {

    const URL_KEY_LIMIT = 'limit';
    const URL_KEY_PAGE = 'page';
    const URL_KEY_ORDER = 'order';
    const URL_KEY_TOKEN  = 'token';
    const URL_KEY_INCLUDE = 'include';

    /**
     * @param array $arr
     * @return bool
     */
    public static function isAssoc($arr)
    {
        if (array() === $arr) return false;
        if (! is_array($arr)) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    /**
     * @param Request $request
     * @return int
     */
    public static function getLimitOfRequest(Request $request)
    {
        $limit = ($request->query->get(self::URL_KEY_LIMIT)) ? $request->query->get(self::URL_KEY_LIMIT) : 25 ;

        if ($request->query->has(self::URL_KEY_PAGE)) {
            $page = ($request->query->get(self::URL_KEY_PAGE)) ? $request->query->get(self::URL_KEY_PAGE) : 1 ;
            if ((int) $page === -1) {
                //fetch all
                $limit = 0;
            }
        } elseif ($request->query->has(self::URL_KEY_TOKEN)) {
            $token = ($request->query->get(self::URL_KEY_TOKEN)) ? $request->query->get(self::URL_KEY_TOKEN) : 0;
            if ((int) $token === -1) {
                //fetch all
                $limit = 0;
            }
        }
        return (int) $limit;
    }


    /**
     * @param Request $request
     * @return int
     */
    public static function getOffsetOfRequest(Request $request)
    {
        $limit = UrlHelper::getLimitOfRequest($request);
        //From the beginning
        $offset = 0;

        if ($request->query->has(self::URL_KEY_PAGE)) {
            $page = ($request->query->get(self::URL_KEY_PAGE)) ? $request->query->get(self::URL_KEY_PAGE) : 1 ;
            if ((int) $page !== -1) {
                // NOT fetch all, then need an offset
                if ($page < 1) {
                    $page = 1;
                }
                $offset = $limit * ((int) $page - 1 );
            }
        } elseif ($request->query->has(self::URL_KEY_TOKEN)) {
            $token = ($request->query->get(self::URL_KEY_TOKEN)) ? $request->query->get(self::URL_KEY_TOKEN) : 0 ;
            if ((int) $token !== -1) {
                // Not fetch all, then token value is the offset.
                $offset = $token;
            }
        }

        return $offset;
    }


    /**
     * @param Request $request
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getOrderOfRequest(Request $request)
    {
        $order =  ($request->query->get(self::URL_KEY_ORDER)) ? $request->query->get(self::URL_KEY_ORDER) : [] ;

        $result = [];
        if (UrlHelper::isAssoc($order)) {
            // ?order[first]=asc&order[second]=desc
            foreach ($order as $name => $direction) {
                $result[$name] = strtoupper($direction);
            }
        } else {
            // ?order=fieldname
           $result[$order] = 'ASC';
        }
        return $result;
    }

    /**
     * @param Request $request
     * @return array
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getIncludeOfRequest(Request $request)
    {
        $result = [];
        $includeStr = ($request->query->get(self::URL_KEY_INCLUDE)) ? $request->query->get(self::URL_KEY_INCLUDE) : '';
        if (!empty($includeStr)) {
            $includes = explode(',', $includeStr);
            foreach ($includes as $include) {
                $result[] = $include;
            }
        }
        return $result;
    }

    /**
     * @param Request $request
     * @param array $allowed
     * @return Criteria
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public static function getCriteriaByRequest(Request $request, $allowed = [])
    {
        // TODO: not implemented yet
        return new Criteria();
    }
}
