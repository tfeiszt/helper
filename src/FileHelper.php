<?php

namespace tfeiszt\helper;

/**
 * Class FileHelper
 * @package tfeiszt\helper
 * @author Tamas Feiszt <tamas.feiszt@d3r.com>
 */
class FileHelper
{
    /**
     * @param $path
     * @return string
     */
    public static function setPathSlash($path)
    {
        if (substr($path, -1) != '/') {
            $path = $path . '/';
        }
        return $path;
    }

    /**
     * @param $dir
     * @param bool|false $rm
     * @return bool
     */
    public static function clearDirectory($dir, $rm = false)
    {
        $result = true;
        if (file_exists($dir)) {
            foreach (scandir($dir) as $item) {
                if ($item == '.' || $item == '..') continue;
                if (!(unlink($dir.DIRECTORY_SEPARATOR.$item))){
                    $result = false;
                }
            }
            if ($result && $rm){
                $result = (rmdir($dir));
            }
        }
        return $result;
    }


    /**
     * @param $file
     * @return string
     */
    public static function getVersionizedFileName($file)
    {
        $filename = pathinfo($file, PATHINFO_BASENAME);
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        return str_replace('.' . $extension, '', $filename) . '_' . self::getFileModificationTime($file) . '.' . $extension;
    }

    /**
     * @param $file
     * @return int
     */
    public static function getFileModificationTime($file)
    {
        if (is_file($file)) {
            return filemtime($file);
        } else {
            return 0;
        }
    }

    /**
     * @param $dir
     * @param bool|false $makeItEmpty
     * @return bool
     */
    public static function makeDir($dir, $makeItEmpty = false)
    {
        //does directory exist?
        if (! file_exists($dir)){
            mkdir($dir, 0775, true);

        }
        //delete old files
        if ($makeItEmpty === true) {
            if ($handle = opendir($dir)) {
                while (false !== ($file = readdir($handle))) {
                    unlink(self::setPathSlash($dir) . $file);
                }
            }
        }
        return file_exists($dir);
    }

    /**
     * @param $array
     * @param string $delimiter
     * @param string $enclosure
     * @param bool|false $encloseAll
     * @param bool|false $nullToMysqlNull
     * @param bool|true $header
     * @return string
     */
    public static function assocToCsv( $array, $delimiter = ';', $enclosure = '"', $encloseAll = false, $nullToMysqlNull = false, $header = true )
    {
        if (count($array) > 0) {
            if ($header) {
                $stock = implode($delimiter, array_keys($array[0])) . "\n";
            } else {
                $stock = "" . "\n";
            }

            foreach ($array as $row) {
                $stock = $stock . self::rowArrayToCsv(array_values($row), $delimiter, $enclosure, $encloseAll, $nullToMysqlNull) . "\n";
            }
        } else {
            $stock = "";
        }
        return $stock;
    }

    /**
     * @param $fields
     * @param string $delimiter
     * @param string $enclosure
     * @param bool|false $encloseAll
     * @param bool|false $nullToMysqlNull
     * @return string
     */
    public static function rowArrayToCsv( $fields, $delimiter = ';', $enclosure = '"', $encloseAll = false, $nullToMysqlNull = false )
    {
        $delimiterEsc = preg_quote($delimiter, '/');
        $enclosureEsc = preg_quote($enclosure, '/');

        $output = array();
        foreach ( $fields as $field ) {
            if ($field === null && $nullToMysqlNull) {
                $output[] = 'NULL';
                continue;
            }
            if ( $encloseAll || preg_match( "/(?:$" . $delimiterEsc ."|$" . $enclosureEsc . "|\\s)/", $field ) ) {
                $output[] = $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure;
            }
            else {
                $output[] = $field;
            }
        }

        return implode( $delimiter, $output );
    }

    /**
     * @param $File
     * @param string $delimiter
     * @param string $enclosure
     * @param int $maxlength
     * @return array
     */
    public static function csvToAssoc($File, $delimiter = ";", $enclosure = "\"", $maxlength = 5000)
    {
        if (is_file($File)) {
            $handle = fopen($File, "r");
            $fields = fgetcsv($handle, $maxlength, $delimiter, $enclosure);
            $stock = array();
            $y = 0;
            while ($data = fgetcsv($handle, $maxlength, $delimiter, $enclosure)) {
                $x = 0;
                foreach ($data as $value) {
                    if (count($fields) > $x) {
                        $stock[$y][$fields[$x]] = $value;
                    }
                    $x++;
                }
                $y++;
            }
            fclose($handle);
        } else {
            $stock = '';
        }
        return $stock;
    }

    /**
     * @param $fName
     * @param string $stringData
     * @return bool
     */
    public static function appendToFile($fName, $stringData = "")
    {
        if ($stringData){
            if (file_exists($fName)){
                $fh = fopen($fName, 'a');
                fwrite($fh, $stringData);
                fclose($fh);
                return TRUE;
            } else
            {
                return self::saveToFile($fName, $stringData);
            }
        }else
        {
            return FALSE;
        }
    }

    /**
     * @param $fName
     * @param string $stringData
     * @param bool $createDirIfNecessary
     * @return bool
     */
    public static function saveToFile($fName, $stringData = "", $createDirIfNecessary = true)
    {
        if ($stringData){
            if ($createDirIfNecessary === true){
                $dirName = pathinfo($fName, PATHINFO_DIRNAME);
                self::makeDir($dirName);
            }
            $fh = fopen($fName, 'w');
            fwrite($fh, $stringData);
            fclose($fh);
            @chmod($fName, 0777);
            return file_exists($fName);
        }else
        {
            return FALSE;
        }
    }


    /**
     * @param $sourceFile
     * @param $targetFile
     * @return string|array
     */
    public static function copyAssetIfNecessary($sourceFile, $targetFile)
    {
        if (strpos($sourceFile, '*.') !== false) {
            $result = [];
            $files = glob($sourceFile);
            foreach($files as $file) {
                $result[] = self::copyAssetIfNecessary($file, self::setPathSlash(str_replace (substr($targetFile, strpos($targetFile, '*.') -1)  , '', $targetFile)) . pathinfo($file, PATHINFO_FILENAME) . '.' . pathinfo($file, PATHINFO_EXTENSION));
            }
            return $result;
        } else {
            $targetDir = self::setPathSlash(pathinfo($targetFile, PATHINFO_DIRNAME));
            $vFileName = $targetDir . self::getVersionizedFileName($sourceFile);
            if (! is_file($vFileName)) {
                self::makeDir($targetDir);
                copy($sourceFile, $vFileName );
            }
            return $vFileName;
        }

    }

    /**
     * @param $sourceFile
     * @param $targetFile
     * @return string|array
     */
    public static function copyAssetAnyway($sourceFile, $targetFile)
    {
        if (strpos($sourceFile, '*.') !== false) {
            $result = [];
            $files = glob($sourceFile);
            foreach($files as $file) {
                $result[] = self::copyAssetAnyway($file, self::setPathSlash(str_replace (substr($targetFile, strpos($targetFile, '*.') -1)  , '', $targetFile)) . pathinfo($file, PATHINFO_FILENAME) . '.' . pathinfo($file, PATHINFO_EXTENSION));
            }
            return $result;
        } else {
            $targetDir = self::setPathSlash(pathinfo($targetFile, PATHINFO_DIRNAME));
            if (!is_file($targetFile)) {
                self::makeDir($targetDir);
                copy($sourceFile, $targetFile);
            }
            return $targetFile;
        }
    }
}

