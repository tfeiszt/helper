# Helper 

### PHP library description
Helper package

  
### Install by composer
```php
{
  "repositories": [
    {
      "type": "vcs",
      "url":  "https://tfeiszt@bitbucket.org/tfeiszt/helper.git"
    }
  ],
  "require": {
    "tfeiszt/helper": "dev-master"
  }
}
```
 
### License

MIT
